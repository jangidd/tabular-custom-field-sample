Tabular Custom-Field Type for Jira
---------------------------------

#### This is just a sample add-on, and commercial usage is not recommended.

  * For ease of creating table/grid, [Hansontable](https://handsontable.com/) is used
  * For more details ask me on Atlassian Community - [My Profile](https://community.atlassian.com/t5/user/viewprofilepage/user-id/690016)


Here are the SDK commands you'll use immediately:

  * `atlas-run`   -- installs this plugin into the product and starts it on localhost
  * `atlas-debug` -- same as atlas-run, but allows a debugger to attach at port 5005
  * `atlas-help`  -- prints description for all commands in the SDK
