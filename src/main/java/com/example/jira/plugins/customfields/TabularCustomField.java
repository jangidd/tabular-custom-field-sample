package com.example.jira.plugins.customfields;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Map;

@Scanned
public class TabularCustomField extends GenericTextCFType
{
  private static final Logger log = LoggerFactory.getLogger(TabularCustomField.class);

  public TabularCustomField(@JiraImport CustomFieldValuePersister customFieldValuePersister,
                            @JiraImport GenericConfigManager genericConfigManager,
                            @JiraImport TextFieldCharacterLengthValidator textFieldCharacterLengthValidator,
                            @JiraImport JiraAuthenticationContext jiraAuthenticationContext
  )
  {
    super(customFieldValuePersister, genericConfigManager, textFieldCharacterLengthValidator,
        jiraAuthenticationContext);
  }

  @Nonnull
  @Override
  protected PersistenceFieldType getDatabaseType()
  {
    return PersistenceFieldType.TYPE_UNLIMITED_TEXT;
  }

  @Override
  public Map<String, Object> getVelocityParameters(final Issue issue,
                                                   final CustomField field,
                                                   final FieldLayoutItem fieldLayoutItem)
  {
    final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);
    if (issue != null) {
      String value = this.getValueFromIssue(field, issue);
      map.put("fieldValue", value);
    } else {
      map.put("fieldValue", null);
    }

    return map;
  }
}